package com.example.rc.samples;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by RENT on 2017-04-19.
 */

public class Model {
    private String name;
    private Double temperature;
    private Integer clouds;
    private String description;

    public Model(String name, Double temperature, Integer clouds, String description) {
        this.name = name;
        this.temperature = temperature;
        this.clouds = clouds;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public Double getTemperature() {
        return temperature;
    }

    public Integer getClouds() {
        return clouds;
    }

    public String getDescription() {
        return description;
    }

    public static Model deserialize(String result) {
        Model model = null;
        JSONObject string = null;
        try {
            string = new JSONObject(result);
            String name = string.getString("name");
            Double temp = string.getJSONObject("main").getDouble("temp");
            String desc = ((JSONObject) string.getJSONArray("weather").get(0)).getString("description");
            Integer clouds = string.getJSONObject("clouds").getInt("all");
            return new Model(name, temp, clouds, desc);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String toString() {
        return "Model{" +
                "name='" + name + '\'' +
                ", temperature=" + temperature +
                ", clouds=" + clouds +
                ", description='" + description + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Model model = (Model) o;

        if (name != null ? !name.equals(model.name) : model.name != null) return false;
        if (temperature != null ? !temperature.equals(model.temperature) : model.temperature != null)
            return false;
        if (clouds != null ? !clouds.equals(model.clouds) : model.clouds != null) return false;
        return description != null ? description.equals(model.description) : model.description == null;

    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (temperature != null ? temperature.hashCode() : 0);
        result = 31 * result + (clouds != null ? clouds.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }
}
