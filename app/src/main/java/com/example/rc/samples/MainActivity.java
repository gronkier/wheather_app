package com.example.rc.samples;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.commons.io.IOUtils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class MainActivity extends AppCompatActivity {
    private Unbinder bind;
    @BindView(R.id.city_name)
    protected EditText cityName;
    @BindView(R.id.imageView)
    protected ImageView weather_icon;
    @BindView(R.id.city_name_text)
    protected TextView city_name_text;
    @BindView(R.id.description)
    protected TextView description;
    @BindView(R.id.temperature)
    protected TextView temperature;
    @BindView(R.id.outBox)
    protected LinearLayout outBox;
    protected Model model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bind = ButterKnife.bind(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        bind.unbind();
    }

    @OnClick(R.id.submit)
    public void submitForm() {
        if (cityName.getText().length() == 0) {
            cityName.requestFocus();
            cityName.setError("To pole nie może być puste");

        } else {
            loadWeather(cityName.getText().toString());
            InputMethodManager inputManager = (InputMethodManager)
                    getSystemService(Context.INPUT_METHOD_SERVICE);

            inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }


    public void loadWeather(final String city) {

        new AsyncTask<Void, Void, Void>() {
            //ProgressDialog loader = new ProgressDialog(this,"Czekaj...", true);

            @Override
            protected void onPreExecute() {
                super.onPreExecute();

            }

            @Override
            protected Void doInBackground(Void... params) {
                try {

                    URL url = new URL("http://api.openweathermap.org/data/2.5/weather?q=" + city + "&APPID=ea574594b9d36ab688642d5fbeab847e&lang=pl&units=metric");

                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                    BufferedReader reader =
                            new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    String stringResult = IOUtils.toString(reader);
//                    StringBuffer json = new StringBuffer(1024);
//                    String tmp = "";
//                    while ((tmp = reader.readLine()) != null)
//                        json.append(tmp).append("\n");
//                    reader.close();

                    model = Model.deserialize(stringResult);


                } catch (Exception e) {

                    System.out.println("Exception " + e.getMessage());
                    return null;
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void Void) {
                if (model != null) {
                    if (CheckNetwork.isInternetAvailable(MainActivity.this)) {
                        outBox(model);

                    } else {
                        Toast.makeText(MainActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
                        Intent gpsOptionsIntent = new Intent(android.provider.Settings.ACTION_WIFI_SETTINGS);
                        startActivityForResult(gpsOptionsIntent, 0);
                    }
                }
            }
        }.execute();

    }

    private void outBox(Model result) {
        outBox.setVisibility(View.VISIBLE);
        city_name_text.setText(model.getName());
        temperature.setText(model.getTemperature().toString() + "°C");
        description.setText(model.getDescription());

        if (model.getClouds() < 70) {
            weather_icon.setImageResource(R.drawable.weather_pouring);
        }
        if (model.getClouds() < 20) {
            weather_icon.setImageResource(R.drawable.weather_sunny);
        }
        if (model.getClouds() < 50) {
            weather_icon.setImageResource(R.drawable.weather_partlycloudy);
        } else {
            weather_icon.setImageResource(R.drawable.weather_sunset);
        }

    }

}
